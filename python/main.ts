//% color="#4169E1" iconWidth=50 iconHeight=40
namespace gravitygnss{

    //% block="GNSS module initialization until successful I2C mode address 0x20" blockType="command" 
    export function init(parameter: any, block: any) {

        Generator.addImport(`import sys\nsys.path.append("/root/mindplus/.lib/thirdExtension/liliang-gravitygnss-thirdex")\nfrom DFRobot_GNSS import *`);
        Generator.addCode(`gnss = DFRobot_GNSS_I2C (bus=0, addr=0x20)
while (gnss.begin() == False):
    print("Sensor initialize failed!!")
    time.sleep(1)
gnss.enable_power()
gnss.set_gnss(GPS_BeiDou_GLONASS)
gnss.rgb_on()
gnss_utc = struct_utc_tim()
gnss_lat_lon = struct_lat_lon()
`);
   
    }
/*无效
   //% block="Receive satellite data?" blockType="boolean"
    export function available(parameter: any, block: any) { 
        Generator.addCode( [`not gnss.get_gnss_len()`,Generator.ORDER_UNARY_POSTFIX]);
   
   } 
*/
    //% block="Read the data once and save it and get the result" blockType="command"
    export function readdata(parameter: any, block: any) { 
        
        Generator.addCode(`gnss_utc = gnss.get_date()
gnss_utc = gnss.get_utc()
gnss_lat_lon = gnss.get_lat()
gnss_lat_lon = gnss.get_lon()
gnss_alt = gnss.get_alt()
gnss_cog = gnss.get_cog()
gnss_sog = gnss.get_sog()
gnss_num = gnss.get_num_sta_used()
gnss_star = gnss.get_gnss_mode()`);
   
   }

    //% block="Fetch [DATE] from result" blockType="reporter"
    //% DATE.shadow="dropdown"   DATE.options="DATE"
    export function getDate(parameter: any, block: any) { 
        let data = parameter.DATE.code;
        Generator.addCode( [`${data}`,Generator.ORDER_UNARY_POSTFIX]);
   
   } 

   //% block="Fetch [DATA] from result" blockType="reporter"
   //% DATA.shadow="dropdown"   DATA.options="DATA"
   export function getData(parameter: any, block: any) { 
       let data = parameter.DATA.code;
       Generator.addCode( [`${data}`,Generator.ORDER_UNARY_POSTFIX]);
  
  } 



}