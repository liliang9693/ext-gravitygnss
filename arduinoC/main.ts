//% color="#4169E1" iconWidth=50 iconHeight=40
namespace gravitygnss{

    //% block="GNSS module initialization until successful I2C mode address 0x20" blockType="command" 
    export function i2cInit(parameter: any, block: any) {

        Generator.addInclude("includeDFRobot_GNSS",`#include "DFRobot_GNSS.h"`);
        Generator.addObject(`gnssobject`, `DFRobot_GNSS_I2C`, `gnss(&Wire ,GNSS_DEVICE_ADDR);`);
        Generator.addSetup("I2Csetup",`while(!gnss.begin()){
    Serial.println("NO Deivces !");
    delay(1000);
  }
  gnss.enablePower();
  gnss.setGnss(eGPS_BeiDou_GLONASS);
  gnss.setRgbOn();
`);
   
    }

    //% board="arduino,leonardo,arduinonano,mega2560,esp32,firebeetleesp32,firebeetleesp32e,telloesp32"
    //% block="GNSS module initialization until successful [HS] mode RX (green): [RX] TX (blue): [TX]  " blockType="command" 
    //% HS.shadow="dropdown" HS.options="HS"
    //% RX.shadow="dropdown" RX.options="RX"
    //% TX.shadow="dropdown" TX.options="TX" 
    export function HsInit(parameter: any, block: any) {
        let hs = parameter.HS.code;
        let rx = parameter.RX.code;
        let tx = parameter.TX.code;
        Generator.addInclude("includeDFRobot_GNSS",`#include "DFRobot_GNSS.h"`);
        Generator.addObject(`hsobj`, `DFRobot_GNSS_UART`, `gnss(&${hs},9600,${rx},${tx});`);
        Generator.addSetup("hssetup",`gnss.begin();`);
   }

   //% board="arduino,leonardo,arduinonano,mega2560,microbit,maqueen,maxbot"
   //% block="GNSS module initialization until successful [SS] mode RX (green): [SRX] TX (blue): [STX]" blockType="command" 
    //% SS.shadow="dropdown" SS.options="SS"
    //% SRX.shadow="dropdown" SRX.options="SRX"    
    //% STX.shadow="dropdown" STX.options="STX"
   export function SsInit(parameter: any, block: any) {
        let ss = parameter.SS.code;
        let rx = parameter.SRX.code;
        let tx = parameter.STX.code;

       Generator.addInclude("includeDFRobot_GNSS",`#include "DFRobot_GNSS.h"`);

       Generator.addObject("SoftwareSerial","SoftwareSerial",`${ss}(${rx}, ${tx});`);
       Generator.addObject("ssobj","DFRobot_GNSS_UART",`gnss(&${ss},9600);`);
       Generator.addSetup("sssetup",`gnss.begin();`);
}



    //% block="Read the data once and save it and get the result" blockType="command"
    export function readdata(parameter: any, block: any) { 
        
        Generator.addCode(`sTim_t utc = gnss.getUTC();
  sTim_t date = gnss.getDate();
  sLonLat_t lat = gnss.getLat();
  sLonLat_t lon = gnss.getLon();
  double high = gnss.getAlt();
  uint8_t starUserd = gnss.getNumSatUsed();
  double sog = gnss.getSog();
  double cog = gnss.getCog();
`)
  }

    //% block="Fetch [DATE] from result" blockType="reporter"
    //% DATE.shadow="dropdown"   DATE.options="DATE"
    export function getDate(parameter: any, block: any) { 
        let data = parameter.DATE.code;
        Generator.addCode( [`${data}`,Generator.ORDER_UNARY_POSTFIX]);
   
   } 

   //% block="Fetch [DATA] from result" blockType="reporter"
   //% DATA.shadow="dropdown"   DATA.options="DATA"
   export function getData(parameter: any, block: any) { 
       let data = parameter.DATA.code;
       Generator.addCode( [`${data}`,Generator.ORDER_UNARY_POSTFIX]);
  
  } 



}