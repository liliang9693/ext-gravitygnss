# Gravity: GNSS北斗定位模块

![](./python/_images/featured.png)

# 说明
Mind+1.7.3及以上版本，GNSS北斗定位模块（TEL0157）的积木库。

商品链接： https://www.dfrobot.com.cn/goods-3652.html

# 积木
- **arduinoC模式：**

![](./arduinoC/_images/blocks.png)

# 程序实例

![](./arduinoC/_images/example.png)



- **Python模式：**

![](./python/_images/blocks.png)

# 程序实例



- **Python模式：**

![](./python/_images/example.png)



# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|Python|备注|
|-----|-----|:-----:|-----|-----|-----|
|uno|    |  √  |    |    |    |
|micro:bit|    |   √ |    |    |    |
|mpython|    |  √    |  |    |    |
|arduinonano|    |  √  |    |    |    |
|leonardo|    |   √ |    |    |    |
|mega2560|    |  √  |    |    |    |
|unihiker|    |    |    |  √  |    |

# 更新日志

* V0.0.1 基础功能完成，仅Python模式
* V0.0.2 增加ArduinoC支持,未完整测试
* V0.0.3 支持arduinoC模式
* V0.0.4 python模式增加库路径 
* V0.0.5 解决arduinoC模式经纬度错乱问题
* V0.0.6 解决显示字符问题
* V0.0.7 解决串口编译出错问题
